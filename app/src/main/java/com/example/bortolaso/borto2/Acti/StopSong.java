package com.example.bortolaso.borto2.Acti;

import android.app.Activity;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.bortolaso.borto2.Acti.MainActivity;
import com.example.bortolaso.borto2.R;


/**
 * Created by bortolaso on 30/07/2016.
 */
public class StopSong extends Activity {
    public static Ringtone ringtoneAlarm;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stop_song);
        ImageView stop = (ImageView) findViewById(R.id.stop);
        stop.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
    public void onBackPressed() {
        ringtoneAlarm.stop();
        finish();
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
    }
    public void onStart(){
        Uri alarmTone = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        ringtoneAlarm = RingtoneManager.getRingtone(this, alarmTone);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ringtoneAlarm.setAudioAttributes(new AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_ALARM).build());
        } else {
            ringtoneAlarm.setStreamType(AudioManager.STREAM_ALARM);
        }
        ringtoneAlarm.play();
        super.onStart();
    }
}
