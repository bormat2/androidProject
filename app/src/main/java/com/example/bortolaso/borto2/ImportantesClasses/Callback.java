package com.example.bortolaso.borto2.ImportantesClasses;

import android.app.Activity;
import android.os.Looper;

/**
 * Created by bortolaso on 04/10/2016.
 */
public abstract class Callback implements Runnable {
    public abstract Activity getActivityContext();
    public void runAfterXMilisecond(final int milliSecond){

        final Activity act = this.getActivityContext();

        new Thread(new Runnable() {
            public void run() {
                sleep(milliSecond);
                if (act == null){
                    if (Looper.myLooper() == null)
                    {
                        Looper.prepare();
                    }
                    this.run();
                    Looper.loop();
                }else{
                    act.runOnUiThread(this);
                }
            }
        }).start();
    }

    private void sleep(int time){
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //spécifique au gps
    public  boolean isGpsProviderForced(){
        return false;
    };
}
