package com.example.bortolaso.borto2.ImportantesClasses;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.Semaphore;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.R.id.message;
import static android.location.LocationManager.GPS_PROVIDER;
import static android.location.LocationManager.NETWORK_PROVIDER;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


/**
 * Created by bortolaso on 27/08/2016.
 */
public class LocationManagerFactory {
    LocationManager _lm;
    LocationManager _lm2;
    Context _context;
    private Location _lastKnownLocation;
    private Date _lastDateUpdate;
    private LocationManagerFactory _;
    private String _link;
    private final ArrayList<Callback> _callbacks = new ArrayList<>();
    private String _provider;
  //  private final Lock _mutex = new ReentrantLock(true);

    public LocationManagerFactory(Context context) {
        _ = this;
        _context = context;
        _lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        _lm2 = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    }

    //mutext empeche des accès concurrent à l'arraylist
    public void addCallback(Callback runnable) {
       // _mutex.lock();
        _callbacks.add(runnable);
     //   _mutex.unlock();

    }

    public void executeAllCallback() {
        Log.d("executeAllCallback","yes");

   //     _mutex.lock();
        if(_provider.equals(GPS_PROVIDER)){
            for (int i = _callbacks.size(); i--> 0; ) {
                _callbacks.get(i).run();
            }
            _callbacks.clear();
            Log.d("callbackIsDone","yes");
        }else{
            ArrayList<Callback> toRemove = new ArrayList<>();
            for (int i = _callbacks.size(); i--> 0; ) {
                Callback c = _callbacks.get(i);
                Log.d("gpsForce",""+c.isGpsProviderForced());
                if(c.isGpsProviderForced()) continue;
                c.run();
                toRemove.add(c);
            }
            _callbacks.removeAll(toRemove);
        }
     //   _mutex.unlock();
    }

    @SuppressWarnings("MissingPermission")
    public Location getLocation() {
        return _lastKnownLocation;
    }

    public static final SimpleDateFormat formater = new SimpleDateFormat("dd MMMM yyyy 'à' HH:mm:ss");

    public String getLinkGMap() {
        String message = "";
        Location location = _.getLocation();
        if (location != null) {
            message += _link;
        } else {
            message += "Application \"T ou\": GPS et réseau inactif, impossible de géolocaliser cette personne";
        }
        return message;
    }

    public String getMessage() {
        String message = "";
        if (getLocation() != null) {
            message += "hello, à la date " + formater.format(_lastDateUpdate) + " ce contact était à " + _link
                    + " d'après la localisation ";
            if(_provider.equals(GPS_PROVIDER)){
                message += "gps";
            }else{
                message += "du réseau mobile/wifi"
                        + "\nPour forcer une localisation GPS ce qui peut durer plus de 30 min voire ne pas aboutir "
                        + "en cas de mauvaise connectivité envoyez #gps en plus du code de géolocalisation";
            }
        } else {
            message += "gps inactif et pas d'historique disponible, la position ne peut pas être déterminée";
        }
        return message;
    }

    private static final long MINIMUM_DISTANCE_CHANGE_FOR_UPDATES = 0; // in Meters
    private static final long MINIMUM_TIME_BETWEEN_UPDATES = 0; // in Milliseconds

    /**
     *
     * @param runnable callback to execute after the update of location
     */
    public void updateLocationAndRunCallback(final Callback runnable) {
        addCallback(runnable);
           /*Criteria criteria = new Criteria();
            final String bestProvider = _lm.getBestProvider(criteria,true);
            Activity act = _context instanceof Activity ? (Activity) _context : null;
            SetTimeout.execute(new Runnable() {
                @Override
                public void run() {
                    requestLocationUpdates(bestProvider);
                }
            }, 5000,act);*/

        Permission.ifPerms(_context, new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION}, new Runnable() {
                @Override
                public void run() {
                    boolean isGps = _lm.isProviderEnabled(GPS_PROVIDER);
                    boolean isNetwork = _lm.isProviderEnabled(NETWORK_PROVIDER);
                    if (isGps) {
                       Toast.makeText(_context, "gps will ask pos", Toast.LENGTH_LONG).show();
                        Log.wtf("here", "isGPs");
                       final LocationListener req = requestLocationUpdates(GPS_PROVIDER);
                   //     _mutex.lock();
                        Log.wtf("test2","tt");
                        (new  Callback() {
                            @Override
                            public Activity getActivityContext(){
                                //  return _context instanceof Activity ?(Activity)  _context : null;
                                return null;
                            }
                            @Override
                            public void run() {
                                //_lm.removeUpdates(req);
                                // si au bout de 20 secondes on a pas encore envoyé le message
                                // récupérer par internet la position
                                Log.wtf("here111", "isSetTimeout");
                                // requestLocationUpdates(NETWORK_PROVIDER);
                            }
                        }).runAfterXMilisecond(10000);

                        if (isNetwork && _callbacks.size() > 0) {
                          /*  Toast.makeText(_context, "network will ask pos", Toast.LENGTH_LONG).show();
                           (new  Callback() {
                               @Override
                               public Activity getActivityContext(){
                                 //  return _context instanceof Activity ?(Activity)  _context : null;
                                   return null;
                               }
                                @Override
                                public void run() {
                                    //_lm.removeUpdates(req);
                                    // si au bout de 20 secondes on a pas encore envoyé le message
                                    // récupérer par internet la position
                                    Log.d("here", "isSetTimeout");
                                   // requestLocationUpdates(NETWORK_PROVIDER);
                                }
                            }).runAfterXMilisecond(10000);*/
                        }
                   //     _mutex.unlock();
                    } else if (isNetwork) {
                        requestLocationUpdates(NETWORK_PROVIDER);
                    }
                    Log.d("perms", "ok");
                }
        });
    }

    public LocationListener requestLocationUpdates(final String provider) {
        Log.d("prov", provider);
        /**
         * It is the class that update the date and location when they change
         * once fields update the listener is delete
         * we call the listener only when an action ask to update the location with onSucess
         * that execute an callback once the location changed
         */

        LocationListener listener = new LocationListener() {
            public void onLocationChanged(Location location) {
               // Log.d("location", "changed");
                _lastKnownLocation = location;
                Toast.makeText(_context, "location update", Toast.LENGTH_LONG).show();
                _link = "http://maps.google.com/?q=" + location.getLatitude()
                        + "," + location.getLongitude();
                _lastDateUpdate = new Date();
                _provider = provider;
                executeAllCallback();
                Toast.makeText(_context, "callback", Toast.LENGTH_LONG).show();

                //    Log.d("executed", " callback");
                //noinspection MissingPermission
                _lm.removeUpdates(this);
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle b) {
            }

            @Override
            public void onProviderDisabled(String s) {
            }

            @Override
            public void onProviderEnabled(String s) {
            }
        };
        //noinspection MissingPermission
        (provider.equals(GPS_PROVIDER) ? _lm : _lm2).requestLocationUpdates(
                provider,
                MINIMUM_TIME_BETWEEN_UPDATES,
                MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
                listener
        );
        return listener;
    }


}
