package com.example.bortolaso.borto2.ImportantesClasses;

import android.content.Context;
import android.telephony.SmsManager;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by bortolaso on 23/07/2016.
 */
public class SMSFactory {
    private SmsManager _smsManager;
    public SMSFactory(){
        _smsManager= SmsManager.getDefault();
    }
    public void sendMessage(final String number, String msg){
        _smsManager.sendMultipartTextMessage(number, null,divide(msg), null, null);
    }

    /**
     * On coupe la chaine en morceaux 65 sinon après ça lag trop lors de l'envoie.
     * @param msg
     * @return
     */
    public  ArrayList<String> divide(String msg){
        final ArrayList<String> messageParts = new ArrayList<>();
        for(int i = 0;i<msg.length();i+=65){
            messageParts.add(msg.substring(i,Math.min(i+65,msg.length())));
        }
        return messageParts;
    }
    public void sendMessage(ArrayList<String> numbers , String msg){
        ArrayList sms_s = divide(msg);
        for (String num: numbers){
            _smsManager.sendMultipartTextMessage(num, null,sms_s, null, null);
        }
    }

    public void sendMessage(final Object numbers, final String message, final int nbOccurence) {
        final boolean isString = numbers instanceof String;
        for (int i = nbOccurence; i-- > 0; ){
            if (isString) {
                sendMessage((String) numbers, message);
            } else {
                sendMessage((ArrayList) numbers, message);
            }
        }
    }
}
