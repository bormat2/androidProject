package com.example.bortolaso.borto2.Acti;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.bortolaso.borto2.ImportantesClasses.Permission;
import com.example.bortolaso.borto2.R;


/*select item.condition.text,  item.condition.temp from weather.forecast where woeid in (select woeid from geo.places(1) where text="nome, ak")*/
public class PermissionActivity extends AppCompatActivity {
    private Permission _p;
    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.permission_activity);
        int id = getIntent().getExtras().getInt("idCallBack");
        Log.d("id",""+id);
        try {
            _p = Permission.get(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        _p.setActivity(this);
        _p.requestPermissions();
    }


    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        _p.onRequestPermissionsResult(requestCode,permissions,grantResults);
        this.finish();
    }

}
