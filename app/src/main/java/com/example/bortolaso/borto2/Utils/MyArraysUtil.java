package com.example.bortolaso.borto2.Utils;

/**
 * Created by bortolaso on 03/09/2016.
 */
public class MyArraysUtil {
    public static int indexOf(int[] arr, int val){
        for(int i = 0;i<arr.length ;i++){
            if(arr[i] == val) return i;
        }
        return -1;
    }
    public static boolean contains (int[] arr, int val){
        for(int i = 0;i<arr.length ;i++){
            if(arr[i] == val) return true;
        }
        return false;
    }

    public static int indexOf(Object[] arr, Object val){
        for(int i = 0;i<arr.length ;i++){
            if(arr[i].equals(val)) return i;
        }
        return -1;
    }
    public static boolean contains (Object[] arr, Object val){
        for(int i = 0;i<arr.length ;i++){
            if(arr[i].equals(val)) return true;
        }
        return false;
    }
}
