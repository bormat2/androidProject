package com.example.bortolaso.borto2.ImportantesClasses;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Process;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.example.bortolaso.borto2.Acti.PermissionActivity;
import com.example.bortolaso.borto2.Utils.MyArraysUtil;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by bortolaso on 27/08/2016.
 */
public class Permission {
    private static HashMap<Integer,Permission> static_instances = new HashMap<>();
    public static int static_lastId = 0;
    public static Permission get(int id) throws Exception {
        Permission perm = static_instances.get(id);
        if(perm == null){
            throw new Exception("error");
        }
        return static_instances.get(id);
    }


    private int _id;
    private Runnable _runnable;
    private Activity _act;
    private Context _cont;
    private String[] _permsToRequest;
    public static void ifPerms(Context context,String[] perms,Runnable runnable){
        Permission p = new Permission();
        p._id = ++static_lastId;
        static_instances.put(static_lastId,p);
        p._runnable = runnable;
        if(context instanceof Activity){
            p._act = (Activity) context;
        }
        p._cont = context;
        p.askPermsIfNeeded(perms);
    }
    public ArrayList getNeededPerms(String[] permissions) {
        ArrayList<String> permsToAsk = new ArrayList<>();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) return permsToAsk;
        for (String permission : permissions) {
            if (ActivityCompat.checkSelfPermission(_cont, permission) != PackageManager.PERMISSION_GRANTED) {
                permsToAsk.add(permission);
            }
        }
        return permsToAsk;
    }

    public void askPermsIfNeeded(String[] permissions) {
        ArrayList<String> perms = getNeededPerms(permissions);
        int length = perms.size();
        _permsToRequest = perms.toArray(new String[length]);
        if (length > 0 && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(_act == null){
                Intent permActivity = new Intent(_cont, PermissionActivity.class);
                permActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                permActivity.putExtra("idCallBack",_id);
                _cont.startActivity(permActivity);
            }else{
                requestPermissions();
            }
        }else{
            launchSuccess(this._id);
        }
    }
    public void setActivity(Activity act){
        _act = act;
    }
    @SuppressLint("NewApi")
    public void requestPermissions(){
        _act.requestPermissions(_permsToRequest,this._id);
    }
    public static void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (MyArraysUtil.contains(grantResults,-1)) {
            launchFailed(requestCode);
        }else{
            launchSuccess(requestCode);
        }
    }

    /**callback*/
    public static void launchSuccess(int id){
        Permission p  = static_instances.get(id);
        if(p._act != null) {
            Toast.makeText(p._act,"Permission successfully granted", Toast.LENGTH_LONG).show();
        }
        static_instances.remove(id);
        p._runnable.run();
        Log.d("runnable","end");
    }

    public static void launchFailed(int id){
        Log.d("failed",""+id);
        Permission p  = static_instances.get(id);
        if(p._act != null) {
            Toast.makeText(p._act, "You refused permission, application will close", Toast.LENGTH_LONG).show();
        }
        static_instances.remove(id);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Process.killProcess(Process.myPid());
        System.exit(1);
    }
}