package com.example.bortolaso.borto2.Component;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;

import com.example.bortolaso.borto2.R;

public class EditTextPreferenceCustom extends android.preference.EditTextPreference
{
    private String default_text;

    public EditTextPreferenceCustom(Context context)
    {
        super(context);
    }
    public EditTextPreferenceCustom(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.edit_text_preference_custom);
        String s1 = ta.getString(R.styleable.edit_text_preference_custom_default_val);
        Log.d("sup2",s1 == null ? "null" : s1);
    }

    @Override
    protected void onDialogClosed(boolean positiveResult)
    {
        super.onDialogClosed(positiveResult);
        setSummary(getSummary());
    }

    @Override
    public CharSequence getSummary()
    {
        String s = getText();
        EditTextPreferenceCustom a = this;
        return s != null && s.length() > 0 ? s : this.default_text;
    }
}