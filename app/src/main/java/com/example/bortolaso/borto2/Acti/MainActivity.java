package com.example.bortolaso.borto2.Acti;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bortolaso.borto2.ImportantesClasses.Callback;
import com.example.bortolaso.borto2.ImportantesClasses.LocationManagerFactory;
import com.example.bortolaso.borto2.ImportantesClasses.Permission;
import com.example.bortolaso.borto2.R;
import com.example.bortolaso.borto2.ImportantesClasses.SMSFactory;
import com.example.bortolaso.borto2.Services.ServiceCommunicator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {
    private LocationManagerFactory locationM;
    private MultiAutoCompleteTextView _multiAutoComp;
    private SMSFactory _sms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        locationM = new LocationManagerFactory(this);
        _sms = new SMSFactory();
        setContentView(R.layout.activity_main);
        Manifest.permission p = new Manifest.permission();
        String[] perms= {p.ACCESS_COARSE_LOCATION, p.ACCESS_FINE_LOCATION, p.READ_CONTACTS,
                p.GET_ACCOUNTS, p.WRITE_CONTACTS, p.RECEIVE_SMS, p.READ_SMS, p.SEND_SMS,
                p.READ_PHONE_STATE};
        Permission.ifPerms(this, perms, new Runnable() {
            @Override
            public void run() {
                _startService();
                _createMultiAutoComp();
                EditText customMessage = (EditText) findViewById(R.id.customMessage);
                _createSendPos(customMessage);
                _createLocation(customMessage);
                _createActionBar();
            }
        });

    }

    private void _createActionBar() {
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.custom_action_bar_layout);
        View view =getSupportActionBar().getCustomView();

        ImageButton imageButton= (ImageButton)view.findViewById(R.id.action_bar_back);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ImageButton imageButton2= (ImageButton)view.findViewById(R.id.action_bar_forward);

        imageButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent settings = new Intent(MainActivity.this, Settings2.class);
                settings.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                MainActivity.this.startActivity(settings);
            }
        });


        ActionBar action = getSupportActionBar();
        action.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

        Toolbar toolbar=(Toolbar)action.getCustomView().getParent();
        toolbar.setContentInsetsAbsolute(0, 0);
        toolbar.getContentInsetEnd();
        toolbar.setPadding(0, 0, 0, 0);
    }

    private void _createLocation(final EditText customMessage){
        ImageView position = (ImageView) findViewById(R.id.position);
        assert position != null;
        // Log.d("test","test1");
        // Toast.makeText(this, "hey", Toast.LENGTH_LONG).show();
        position.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, R.string.wait_for_position, Toast.LENGTH_LONG).show();
                locationM.updateLocationAndRunCallback(new Callback(){
                    @Override
                    public Activity getActivityContext(){
                        return MainActivity.this;
                    };

                    @Override
                    public void run() {
                        //  Log.d("test","aaaa");
                        // Log.d("link",locationM.getLinkGMap());
                        customMessage.append(locationM.getLinkGMap());
                    }
                });
            }
        });
    }

    private void _createSendPos(final EditText customMessage){
        final Button sendPos = (Button) findViewById(R.id.sendPos);
        assert sendPos != null;
        sendPos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //extraction des numéros
                ArrayList<String> numbers = new ArrayList<String>();
                String nums = ""+_multiAutoComp.getText();
                String[] a_nums = nums.split(",");
                for(String num : a_nums){
                    Matcher m2 = Pattern.compile("(?:<(.+?)>)").matcher(num);
                    if(m2.find()){
                        numbers.add(m2.group(1));
                    }else{
                        // Que des chiffres et des plusdans les numéros
                        numbers.add(num.replaceAll("[^+\\d]",""));
                    }

                }
                /////////
                // préparation et envoie des smss
                assert customMessage != null;
                TextView nbMessage = (TextView) findViewById(R.id.nbMessage);
                //pas de lettre dans le nombre de message
                int nbMessageValue = Integer.parseInt(0 + nbMessage.getText().toString().replaceAll("\\D+", ""));
                nbMessageValue = Math.max(nbMessageValue, 1);
                _sms.sendMessage(numbers, customMessage.getText() + "", nbMessageValue);
                Toast.makeText(MainActivity.this,  getString(R.string.sms_sent_to_nb,numbers.size()), Toast.LENGTH_LONG).show();
            }
        });
    }
    private void _createMultiAutoComp() {
        _multiAutoComp = (MultiAutoCompleteTextView) findViewById(R.id.multiAutoCompleteTextView1);
        assert _multiAutoComp != null;
        _multiAutoComp.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        SimpleAdapter mAdapter = new SimpleAdapter(this, this.getPeopleList(), R.layout.custcontview,
                new String[]{"Name", "Phone", "Type"}, new int[]{R.id.ccontName, R.id.ccontNo, R.id.ccontType});
        _multiAutoComp.setAdapter(mAdapter);

        _multiAutoComp.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> av, View arg1, int index, long arg3) {
                Map<String, String> map = (Map<String, String>) av.getItemAtPosition(index);
                String name = map.get("Name");
                String number = map.get("Phone");
                String before = "," + _multiAutoComp.getEditableText().toString() + name + "<" + number + ">,";
                before = before.replaceAll("\\{.*\\}", "").replaceFirst(",*\\s*", "").replaceAll(",+", ",");
                _multiAutoComp.getText().clear();
                _multiAutoComp.append(before);
            }
        });
        _multiAutoComp.addTextChangedListener(new TextWatcher() {
            private int mPreviousLength = 0;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0 && s.length() < mPreviousLength) {
                    int endSelection = _multiAutoComp.getSelectionEnd();
                    char end = s.charAt(endSelection < 1 ? 0 : endSelection - 1);
                    if (end == '>' || end == ',') {
                        String contacts = _multiAutoComp.getEditableText().toString();
                        int indexBeforeLastCont = contacts.substring(0, endSelection).lastIndexOf(">");
                        if (indexBeforeLastCont > 0) {
                            indexBeforeLastCont = Math.min(indexBeforeLastCont + 1, s.length() - 1);
                            if (indexBeforeLastCont < 0) indexBeforeLastCont = 0;
                            int indexStartSelection = contacts.substring(0, indexBeforeLastCont).lastIndexOf(",");
                            if (indexStartSelection < 0) {
                                indexStartSelection = 0;
                            } else {
                                indexStartSelection += 1;
                                indexStartSelection = Math.min(indexStartSelection, endSelection);
                            }
                            _multiAutoComp.setSelection(indexStartSelection, endSelection);
                        }
                    }
                }
                mPreviousLength = s.length();
            }
        });
    }


    public void _startService() {
        final Intent myIntent = new Intent(this, ServiceCommunicator.class);
        startService(myIntent);
    }

    public ArrayList<Map<String, String>> getPeopleList() {
        ArrayList<Map<String, String>> mPeopleList = new ArrayList<Map<String, String>>();
        Cursor people = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        assert people != null;
        while (people.moveToNext()) {
            String contactName = people.getString(people.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)),
                    contactId = people.getString(people.getColumnIndex(ContactsContract.Contacts._ID)),
                    hasPhone = people.getString(people.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
            if (Integer.parseInt(hasPhone) > 0) {
                Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
                assert phones != null;
                while (phones.moveToNext()) {
                    //store numbers and display a dialog letting the user select which.
                    String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    String numberType = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                    Map<String, String> NamePhoneType = new HashMap<String, String>();
                    NamePhoneType.put("Name", contactName);
                    NamePhoneType.put("Phone", phoneNumber);
                    switch (numberType) {
                        case "0":
                            NamePhoneType.put("Type", "Work");
                            break;
                        case "1":
                            NamePhoneType.put("Type", "Home");
                            break;
                        case "2":
                            NamePhoneType.put("Type", "Mobile");
                            break;
                        default:
                            NamePhoneType.put("Type", "Other");
                            break;
                    }
                    mPeopleList.add(NamePhoneType);
                }
                phones.close();
            }
        }
        people.close();
        return mPeopleList;
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Permission.onRequestPermissionsResult(requestCode, permissions,grantResults);
    }

    @Override
    public void onStart() {
        if (StopSong.ringtoneAlarm != null) {
            StopSong.ringtoneAlarm.stop();
        }
        super.onStart();
    }

}
